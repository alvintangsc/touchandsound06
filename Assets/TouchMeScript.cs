﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using System;

public class TouchMeScript : MonoBehaviour 
{
	AudioSource gunFireSound;
	// Use this for initialization
	void Start () 
	{	
		gunFireSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{	
	}
	private void OnEnable()
	{
		// subscribe to gesture's Tapped event
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}
	
	private void OnDisable()
	{
		// don't forget to unsubscribe
		GetComponent<TapGesture>().Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, EventArgs e)
	{
		gunFireSound.Play();
	}
}
